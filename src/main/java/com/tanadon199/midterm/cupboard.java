/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.midterm;

import java.util.Scanner; //รับ Import Java Scanner เพื่อรับค่าของข้อมูลผ่านการป้อนข้อมูล

/**
 *
 * @author Kitty
 */
public class cupboard { // Class Cupboard หรือ ตู้เซเว่น

    Scanner kb = new Scanner(System.in); //รับค่าผ่านทางคีบอร์ด
    String namemenu; //สร้างตัวแปร String ชื่อ NameMenu
    double balance; //สร้างตัวแปร Double ชื่อ Balance

    void deposit(double money) { //ประกาศ Deposit เป็น ตัวแปร Money-Double เพื่อรับค่าทศนิยมหรือเลข

        if (money > 0) {  //ถ้าเงินมากกว่า 0 จะสามารถทำรายการต่อไปได้
            this.balance = this.balance + money;
        } else { //ถ้าเงินน้อยกว่า 0  ปริ้นคำสั่งด้านล่าง
            System.out.println("You need to have money");
        }

    }

    void widthdraw(double money) { //ประกาศ Widthdrarw เป็น ตัวแปร Money-Double เพื่อรองรับค่าทศนิยมหรือเลข
        if (money > this.balance) { //ถ้า Money มีเงินมากกว่า Balance ปริ้นคำสั่ง

            System.out.println("Your amount is not enough Please make a new entry");
        } else { 
            this.balance -= money;
        }
    }

    void menu(int menu) { //ประกาศตัวแปร Menu
        if (menu == 1) { //ถ้ารับค่าผ่านทางคีบอร์ด เท่ากับ 1 ทำงานด้านล่างต่อ
            if (this.balance <= 0) { //ถ้าเงินในกระเป๋ามีน้อยกว่าหรือเท่ากับ 0 ปริ้นข้อความด้านล่าง
                System.out.println("You Dont Have Money  Get Back!!!!!!!!");
            } else {
                while (true) { //คำสั่งวนลูปเพิ่อปริ้นข้อความข้างล่างวนไปเรื่อยๆ
                    if (this.balance <= 0) { //ถ้าเงินในกระเป๋ามีน้อยกว่าหรือเท่ากับ 0 ปริ้นข้อความด้านล่าง
                        System.out.println("your money is out!!!");
                        break; //หยุดทำการระบบทั้งหมด
                    } else{ //ถ้าเงินในกระเป๋ามีมากกว่าหรือเท่ากับ 0 ปริ้นข้อความด้านล่าง
                        System.out.println("Press number 1 to order bread  30 balance");
                        System.out.println("Press number 2 to order instant rice 40 balance");
                        System.out.println("Press number 3 to order chicken breast 50 balance");
                        System.out.println("Press number 4 to order cooked fried egg 20 balance");
                        System.out.println("Press number 5 to order Smoked Sausage 60 balance");
                        System.out.println("Press number 6 to Exit");
                        int k1 = kb.nextInt(); //ตัวแปร k1 รับค่าทางผ่านทางคีบอร์ด
                        if (k1 == 1 && this.balance >= 30) { //ถ้าตัวแปร k1 เท่ากับ ตัวแปรที่รับผ่านทางคีบอร์ดเลข 1 และ เงินในกระเป๋ามีค่าน้อยกว่าหรือเท่ากับ 30 ทำรายการต่อ
                            this.balance -= 30; //ลบ เงิน 30 บาท ในกระเป๋า
                            System.out.println("You Got Bread");
                            System.out.println("now the balance" + " " + this.balance);

                        } else if (k1 == 2 && this.balance >= 40) { //ถ้าตัวแปร k1 เท่ากับ ตัวแปรที่รับผ่านทางคีบอร์ดเลข 2 และ เงินในกระเป๋ามีค่าน้อยกว่าหรือเท่ากับ 40 ทำรายการต่อ
                            this.balance -= 40; //ลบเงิน 40 บาท ในกระเป๋า
                            System.out.println("You Got instant rice");
                            System.out.println("now the balance" + " " + this.balance);
                        } else if (k1 == 3 && this.balance >= 50) { //ถ้าตัวแปร k1 เท่ากับ ตัวแปรที่รับผ่านทางคีบอร์ดเลข 3 และ เงินในกระเป๋ามีค่าน้อยกว่าหรือเท่ากับ 50 ทำรายการต่อ
                            this.balance -= 50; //ลบเงิน 50 บาท ในกระเป๋า
                            System.out.println("You Got chicken breast");
                            System.out.println("now the balance" + " " + this.balance);
                        } else if (k1 == 4 && this.balance >= 20) { //ถ้าตัวแปร k1 เท่ากับ ตัวแปรที่รับผ่านทางคีบอร์ดเลข 4 และ เงินในกระเป๋ามีค่าน้อยกว่าหรือเท่ากับ 20 ทำรายการต่อ
                            this.balance -= 20; //ลบเงิน 20 บาท ในกระเป๋า
                            System.out.println("You Got cooked fried egg");
                            System.out.println("now the balance" + " " + this.balance);
                        } else if (k1 == 5 && this.balance >= 60) { //ถ้าตัวแปร k1 เท่ากับ ตัวแปรที่รับผ่านทางคีบอร์ดเลข 5 และ เงินในกระเป๋ามีค่าน้อยกว่าหรือเท่ากับ 60 ทำรายการต่อ
                            this.balance -= 60; //ลบเงิน 20 บาท ในกระเป๋า
                            System.out.println("You Got Smoked Sausage");
                            System.out.println("now the balance" + " " + this.balance);
                        } else if (k1 == 6) { //ถ้าตัวแปร k1 เท่ากับ ตัวแปรที่รับผ่านทางคีบอร์ดเลข 5 ทำรายการต่อ
                            break; //เบรคระบบ
                        }

                    }// 
                }

            }

        } else if (menu == 2) { //ถ้า ป้อนค่าเข้ามาเท่ากับ 2 ลงมาทำรายการข้างล่าง
            System.out.println("the amount you deposit??");
            int k2 = kb.nextInt(); //รับค่าจากทางคีบอร์ดมาใส่ในตัวแปร k2
            this.deposit(k2); // ใช่ฝั่งชั่น Deposit และนำที่รับผ่านทางคีบอร์ด k2 ไปเช็คในฝั่งชั่น Deposit
            System.out.println("now the balance" + " " + this.balance);
        } else if (menu == 3) { //ถ้า ป้อนค่าเข้ามาเท่ากับ 3 ลงมาทำรายการข้างล่าง
            int k3 = kb.nextInt(); //รับค่าจากทางคีบอร์ดมาใส่ในตัวแปร k3
            this.widthdraw(k3); // ใช้ฝั่งชั่น widthdraw และนำที่รับผ่านทางคีบอร์ด k3 ไปเช็คในฝั่งชั่น widthdraw
            System.out.println("now the balance" + " " + this.balance);

        } else if (menu == 4) { 
        } else {
            System.out.println("Please press on the given number 1 2 3 4 ");
        }

    }
}
